export class Song {
    _id: number;
    song_name: string;
    artist: string;
    rating: number;
}