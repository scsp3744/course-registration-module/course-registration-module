import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { AddSongPageRoutingModule } from './add-song-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AddSongPage } from './add-song.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddSongPageRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [AddSongPage]
})
export class AddSongPageModule {}
