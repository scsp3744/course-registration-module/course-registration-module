import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', loadChildren: () => import('./indexhome/home.module').then(m => m.HomePageModule) },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'home2',
    loadChildren: () => import('./home2/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'home3',
    loadChildren: () => import('./home3/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'home4',
    loadChildren: () => import('./home4/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'add-course',
    loadChildren: () => import('./add-coursereg/add-coursereg.module').then(m => m.AddCourseregPageModule)
  },
  {
    path: 'modify-course/:id',
    loadChildren: () => import('./edit-coursereg/edit-coursereg.module').then(m => m.EditCourseregPageModule)
  },
  {
    path: 'modify-course2/:id',
    loadChildren: () => import('./edit-coursereg2/edit-coursereg.module').then(m => m.EditCourseregPageModule)
  },
  {
    path: 'modify-course3/:id',
    loadChildren: () => import('./edit-coursereg3/edit-coursereg.module').then(m => m.EditCourseregPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }