import { Component, OnInit, NgZone } from '@angular/core';
import { CourseregService } from './../shared/coursereg.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from "@angular/forms";


@Component({
  selector: 'app-add-coursereg',
  templateUrl: './add-coursereg.page.html',
  styleUrls: ['./add-coursereg.page.scss'],
})

export class AddCourseregPage implements OnInit {
  courseregForm: FormGroup;

  constructor(
    private courseregAPI: CourseregService,
    private router: Router,
    public fb: FormBuilder,
    private zone: NgZone
  ) {
    this.courseregForm = this.fb.group({
      coursereg_name: [''],
      artist: [''],
      rating: [''],
      course1: [''],
      course2: [''],
      course3: [''],
      course4: ['']
    })
  }

  ngOnInit() { }

  onFormSubmit() {
    if (!this.courseregForm.valid) {
      return false;
    } else {
      this.courseregAPI.addCoursereg(this.courseregForm.value)
        .subscribe((res) => {
          this.zone.run(() => {
            console.log(res)
            this.courseregForm.reset();
            this.router.navigate(['/home3']);
          })
        });
    }
  }
}