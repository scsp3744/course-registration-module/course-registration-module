import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddCourseregPage } from './add-coursereg.page';

const routes: Routes = [
  {
    path: '',
    component: AddCourseregPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddCourseregPageRoutingModule {}
