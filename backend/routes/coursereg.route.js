const express = require('express');
const app = express();
const courseregRoute = express.Router();

let CourseregModel = require('../model/Coursereg');

// Add Coursereg
courseregRoute.route('/create-coursereg').post((req, res, next) => {
  CourseregModel.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
      console.log('Course registered successfully!')
    }
  })
});

// Get all courseregs
courseregRoute.route('/').get((req, res) => {
  CourseregModel.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Get single coursereg
courseregRoute.route('/get-coursereg/:id').get((req, res) => {
  CourseregModel.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})


// Update coursereg
courseregRoute.route('/update-coursereg/:id').put((req, res, next) => {
  CourseregModel.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
      console.log(error)
    } else {
      res.json(data)
      console.log('Registration successfully updated!')
    }
  })
})

// Delete coursereg
courseregRoute.route('/delete-coursereg/:id').delete((req, res, next) => {
  CourseregModel.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: dataS
      })
    }
  })
})

module.exports = courseregRoute;