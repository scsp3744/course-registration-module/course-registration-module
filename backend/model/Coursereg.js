const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Coursereg = new Schema({
  coursereg_name: {
    type: String
  },
  artist: {
    type: String
  },rating: {
    type: String
  },
  course1: {
    type: String
  },
  course2: {
    type: String
  },
  course3: {
    type: String
  },
  course4: {
    type: String
  },
  course5: {
    type: String
  },
  course6: {
    type: String
  },
  status: {
    type: String
  },
}, {
  collection: 'courseregs'
})


module.exports = mongoose.model('Coursereg', Coursereg)